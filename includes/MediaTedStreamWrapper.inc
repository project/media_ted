<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class to handle TED videos.
 */

/**
 * Create an instance like this:
 * @code
 * $ted = new ResourceTedStreamWrapper('ted://v/[video-code]');
 * @endcode
 */
class MediaTedStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'https://www.ted.com/';
  protected $oembed_base = 'http://www.ted.com/services/v1/oembed.json';

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/ted';
  }

  /**
   * Returns the full TED video URL.
   * Used by file_create_url(). The saved video code
   * is in the `talks%2Fvideo_slug` form, thus the urldecode().
   */
  public function interpolateURL() {
    $parts = $this->get_parameters();

    return $this->base_url . check_plain(urldecode($parts['v']));
  }

 /**
  * Retrieves the TED-provided thumbnail.
  */
  public function getOriginalThumbnailPath() {
    $parts = $this->get_parameters();
    $id = check_plain($parts['v']);
    $uri = file_stream_wrapper_uri_normalize('ted://v/' . $id);

    $oembed = $this->getOEmbed($uri);

    if (!empty($oembed['thumbnail_url'])) {
      return $oembed['thumbnail_url'];
    }
  }

  /**
   * Serves video thumbnails from filesystem.
   *
   * Will fetch from origin if it fails to find them locally.
   */
  public function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-ted/' . check_plain($parts['v']) . '.jpg';

    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }

    return $local_path;
  }

  /**
   * Returns oEmbed data.
   */
  public function getOEmbed($uri) {
    $url = file_create_url($uri);
    $oembed_url = url($this->oembed_base, array(
      'query' => array('url' => $url),
    ));
    $response = drupal_http_request($oembed_url);

    if (!isset($response->error)) {
      return drupal_json_decode($response->data);
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }
  }

}
