<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle TED videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetTedHandler extends MediaInternetBaseHandler {
  /**
   * Parses the embed id from a provided embed code.
   */
  public function parse($embedCode) {
    // Check shortcodes.
    if (preg_match('@\[ted id=([0-9]+).*\]@i', $embedCode, $matches) && isset($matches[1])) {
      $id = $matches[1];
      $lang = NULL;

      if (preg_match('@lang=([a-z]{2}).*@i', $embedCode, $matches)) {
        $lang = $matches[1];
      }

      $embedCode = $this->getEmbedFromId($id, $lang);
    }
    $pattern = '@ted\.com\/(talks\/[^\'"]*)@i';

    if (preg_match($pattern, $embedCode, $matches) && isset($matches[1])) {
      return file_stream_wrapper_uri_normalize('ted://v/' . urlencode($matches[1]));
    }
  }

  /**
   * Returns the video URL based on a TED video id.
   * The canonical video id is `talks/video-slug`.
   */
  public function getEmbedFromId($id, $language = NULL) {
    $url = 'http://www.ted.com/talks/' . $id;
    if ($language) {
      $url .= '?language = ' . check_plain($language);
    }

    $oembed = $this->getOEmbed($url);
    if (!empty($oembed['url'])) {
      return $oembed['url'];
    }
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  public function save() {
    $file = $this->getFileObject();
    // If a user enters a duplicate TED URL, the object will be saved again.
    // Set the timestamp to the current time, so that the media item shows up
    // at the top of the media library, where they would expect to see it.
    $file->timestamp = REQUEST_TIME;
    file_save($file);
    return $file;
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);
    $url = file_create_url($uri);

    // Try to default the file name to the video's title.
    if (empty($file->fid) && $info = $this->getOEmbed($url)) {
      $file->filename = truncate_utf8($info['title'], 255);
    }

    return $file;
  }

  /**
   * Returns information about the media. See http://www.oembed.com/.
   *
   * @return
   *   If oEmbed information is available, an array containing 'title', 'type',
   *   'url', and other information as specified by the oEmbed standard.
   *   Otherwise, NULL.
   */
  public function getOEmbed($url = '') {
    if (empty($url)) {
      $uri = $this->parse($this->embedCode);
      $url = file_create_url($uri);
    }

    $oembed_url = url('http://www.ted.com/services/v1/oembed.json', array(
      'query' => array('url' => $url)
    ));
    $response = drupal_http_request($oembed_url);

    if (!isset($response->error)) {
      return drupal_json_decode($response->data);
    }
    else {
      throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
      return;
    }
  }

}
