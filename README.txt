CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 * Features

INTRODUCTION
------------

Current Maintainers:

 * Dan Braghis <http://drupal.org/user/354424>

Media: TED adds TED as a supported media provider.

REQUIREMENTS
------------

Media: TED has one dependency.

Contributed modules
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------

Media: TED can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

USAGE
-----

Media: TED integrates the TED video-sharing service with the Media
module to allow users to add and manage TED videos as they would any other
piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: TED enabled, users can add a TED video by entering its URL
or embed code.

FEATURES
--------

Media: TED works with
* embed codes (e.g. [ted id=1470], [ted id=1817 lang=ro])
* iframe embed code (e.g. <iframe src="https://embed-ssl.ted.com/talks/apollo_robbins_the_art_of_misdirection.html" width="640" height="360" frameborder="0" scrolling="no" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>)
* direct video URLs (e.g. http://www.ted.com/talks/apollo_robbins_the_art_of_misdirection)

To get the embed code:
1. Visit https://www.ted.com and locate the talk you would like to embed.
2. Click on the Embed button at the bottom of the player window.
3. Copy the WordPress.com shortcode from the overlay window and paste it in your select media overlay under the web tab.
