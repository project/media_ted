<?php

/**
 * @file
 * Template file for theme('media_ted_video').
 *
 * Variables available:
 *  $uri - The uri to the Ted video, such as ted://v/xsy7x8c9.
 *  $video_id - The unique identifier of the Ted video.
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Vimeo iframe.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $title - The Media: TED file title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 */
?>
<div class="<?php print $classes ?> media-ted-<?php print $id; ?>">
  <iframe class="media-ted-player" src="<?php print $url ?>" width="<?php print $width ?>" height="<?php print $height ?>" title="<?php print $title; ?>" allowfullscreen frameborder="0" scrolling="no"><?php print $alternative_content; ?></iframe>
</div>
